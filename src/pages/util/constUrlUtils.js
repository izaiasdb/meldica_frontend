//Produção
export const URL_BACKEND = process.env.NODE_ENV === 'production' ? "https://meldica-backend.herokuapp.com" : "http://localhost:8080/meldica_backend";
export const URL_FRONTEND = process.env.NODE_ENV === 'production' ? "https://meldica-frontend.herokuapp.com/" : "http://localhost:8089/";


//Desenvolvimento
// export const URL_BACKEND = "http://localhost:8080/meldica_backend";
// export const URL_FRONTEND = "http://localhost:8089/";

//export const URL_FRONTEND = process.env.NODE_ENV === 'production' ? "https://meldica-frontend.herokuapp.com/" : "http://localhost:8089/";
//DEV VM
//export const URL_BACKEND = process.env.NODE_ENV === 'production' ? "https://meldica-backend.herokuapp.com" : "http://192.168.1.110:8080/meldica_backend";
// DEV Meldica
//export const URL_BACKEND = process.env.NODE_ENV === 'production' ? "https://meldica-backend.herokuapp.com" : "http://192.168.0.20:8080/meldica_backend";


