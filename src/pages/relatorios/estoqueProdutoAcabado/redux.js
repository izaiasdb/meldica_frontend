import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { get } from "lodash"

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  estoqueProdutoAcabadoInit: null,
  estoqueProdutoAcabadoSuccess: ['dados'],
  estoqueProdutoAcabadoFailure: ['msgError'],
  estoqueProdutoAcabadoCleanMessage: null,
  estoqueProdutoAcabadoCleanTable: null,
  estoqueProdutoAcabadoImprimir: ['idEmpresa'],
});

export const EstoqueProdutoAcabadoTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  fetching: false,
});

/* ------------- Reducers ------------- */

export const request = (state) => state.merge({ fetching: true })
export const success = (state, { dados }) => {
  
  let data = {
    list: get(dados, ['list'], get(state.data, ['list'], [])),
    message: get(dados, ['message'], get(state.data, ['message'], [])),
  }

  state = state.merge({ fetching: false, data })
  return state
}

export const failure = (state, { msgError }) => {
  return state.merge({ fetching: false, data: { ...state.data, message: msgError } })
}

export const cleanMessage = (state) => state.merge({ data: { ...state.data, message: "" } })
export const cleanTable = (state) => state.merge({ data: { ...state.data, list: [] } })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ESTOQUE_PRODUTO_ACABADO_INIT]: request,
  [Types.ESTOQUE_PRODUTO_ACABADO_IMPRIMIR]: request,
  [Types.ESTOQUE_PRODUTO_ACABADO_SUCCESS]: success,
  [Types.ESTOQUE_PRODUTO_ACABADO_FAILURE]: failure,
  [Types.ESTOQUE_PRODUTO_ACABADO_CLEAN_MESSAGE]: cleanMessage,
  [Types.ESTOQUE_PRODUTO_ACABADO_CLEAN_TABLE]: cleanTable,
})
