import { call, put } from 'redux-saga/effects'
import Action from './redux';
import { get } from "lodash";
import moment from 'moment';

export function* imprimir(api, { idEmpresa }) {
  try {
    console.log('aqui')
    const response = yield call(api.RelatorioOrdemServico.imprimirEstoqueProdutoAcabado, idEmpresa)
    
    if (response.ok) {
      const report = get(response, ['data'], {})
      const bytes = yield stringToBytes(report);
      const extensao = 'PDF'      
      const date = new moment().format('DDMMYYYY_HHmmss')
      yield saveByteArray(extensao === 'PDF' ? `Produto_Acabado_${date}.pdf` : `Pedido_${date}.xls`, bytes, extensao);
      yield put(Action.estoqueProdutoAcabadoSuccess({}))
    } else {
      const { message } = get(response, ['data'], {})
      yield put(Action.estoqueProdutoAcabadoFailure(message))
    }
  } catch (ex) {
    console.log(ex)
    yield put(Action.estoqueProdutoAcabadoFailure())
  }
}

function* stringToBytes(base64) {
  var binaryString = window.atob(base64)
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
    var ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}

function* saveByteArray(reportName, byte, extensao) {
  let blob = null;
  if (extensao === 'PDF') {
    blob = new Blob([byte], { type: "application/pdf" });
  } else {
    blob = new Blob([byte]);
  }
  let link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  link.download = reportName;
  link.click();
};