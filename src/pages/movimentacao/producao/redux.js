import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { get } from "lodash"
import { SEARCHING } from '../../util/state'
import { MESSAGE_ERROR_DEFAULT } from '../../util/messages'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
   producaoInit: null,
   producaoObter: ['id'],
   producaoSuccess: ['dados'],
   producaoPesquisar: ['producao'],
   producaoFailure: ['message'],
   producaoCleanMessage: null,
   producaoSalvar: ['obj'],
   producaoSetStateView: ['stateView'],
   producaoSetProducao: ['producao'],
   producaoCleanTable: null,
   producaoImprimir : ['obj'],
});

export const ProducaoTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    data:  {},
    fetching: false,
    stateView: SEARCHING,
    producao: null,
});

/* ------------- Reducers ------------- */

export const request = (state) => state.merge({ fetching: true })
export const success = (state, { dados }) =>  {

  let data = {
    list:                       get(dados, ['list'], get(state.data, ['list'], [])),
    message:                    get(dados, ['message'], get(state.data, ['message'], [])),
    funcionarioList:            get(dados, ['funcionarioList'], get(state.data, ['funcionarioList'], [])),    
    produtoList:                get(dados, ['produtoList'], get(state.data, ['produtoList'], [])),
  }

   state = state.merge({fetching: false, data})
   return state
}

export const failure = (state, { message = MESSAGE_ERROR_DEFAULT}) => {
  return state.merge({fetching: false, data: {...state.data, message: {tipo: 'error', descricao: message }}})
}

export const cleanMessage = (state) => state.merge({data: {...state.data, message: ""}})
export const cleanTable = (state) => state.merge({data: {...state.data, list: []}})

export const setStateView = (state, action) => state.merge({stateView: action.stateView})
export const setProducao = (state, { producao }) => state.merge({producao, fetching: false})
export const setProducaoUltimaCompraCliente = (state, { producaoUltimaCompraCliente }) => state.merge({producaoUltimaCompraCliente, fetching: false, drawerUltimaCompraClienteVisivel: true})

export const setProducaoItems = (state, { producaoItems }) => state.merge({
  producao: {
    ...state.producao,
    producaoItems
  }  
})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PRODUCAO_INIT]                                     : request,
  [Types.PRODUCAO_OBTER]                                    : request,
  [Types.PRODUCAO_PESQUISAR]                                : request,
  [Types.PRODUCAO_IMPRIMIR]                                 : request,  
  [Types.PRODUCAO_SUCCESS]                                  : success,
  [Types.PRODUCAO_FAILURE]                                  : failure,
  [Types.PRODUCAO_CLEAN_MESSAGE]                            : cleanMessage,
  [Types.PRODUCAO_SALVAR]                                   : request,
  [Types.PRODUCAO_SET_STATE_VIEW]                           : setStateView,
  [Types.PRODUCAO_SET_PRODUCAO]                        : setProducao,
  [Types.PRODUCAO_CLEAN_TABLE]                              : cleanTable,  
})
