import React from 'react'
import { Row, Col, Form, Select, Button, Table, Icon, Divider, Input, Tag, InputNumber, Card, Switch } from 'antd'
import { isEqual, isNil } from 'lodash'
import { generateOptions } from '../../../util/helper'
import { openNotification } from '../../../util/notification'
import { INSERTING, EDITING, VIEWING } from '../../../util/state'
import { obterPercentualDesconto, obterValorDesconto } from '../../../util/moneyUtils'

const Option = Select.Option

export default class TabProduto extends React.Component {

    state = { 
        produtoDescricao: null,
        viewStateTab: INSERTING,
    }

    adicionar = () => {
        const { 
            form: { getFieldValue, getFieldsValue, setFieldsValue },
            produtoList = [], 
        } = this.props
        const { viewStateTab } = this.state

        let { osProduto } = getFieldsValue(['osProduto'])
        let produtoItemsList = getFieldValue("producao.produtoItemsList")
        let { id, produto, quantidade } = osProduto      
        
        if(!(produto && produto.id && quantidade)){
            openNotification({tipo: 'warning', descricao: 'Por favor, preencha todos os campos referentes ao produto.'})
            return null
        }     

        let recordInserido = produtoItemsList.find(c=> c.produto.id == produto.id)

        if ((isEqual(viewStateTab, INSERTING) && recordInserido) || 
            (isEqual(viewStateTab, EDITING) && recordInserido && recordInserido.id != id)) {
            openNotification({tipo: 'warning', descricao: 'Produto já cadastrado.'})
            return null            
        }      

        if (id){
            let oldRegistro = produtoItemsList.find(c=> c.id == id)

            const index = produtoItemsList.indexOf(oldRegistro);

            //Apaga o anterior
            if (index > -1) {
                produtoItemsList.splice(index, 1);
            }          
        }   

        let produtoForm = produtoList.find(c=> c.id == produto.id);
        osProduto.nomeProduto = produtoForm.nome;
        osProduto.cancelado = false;
        //osProduto.quantidade = quantidade;
        //osProduto.qtdEstoqueCaixa = produtoForm.qtdEstoqueCaixa;
        //osProduto.qtdEstoqueUnidade = produtoForm.qtdEstoqueUnidade;
        osProduto.quantidadeNaCaixa = produtoForm.quantidadeNaCaixa;
        osProduto.fracionado = produtoForm.fracionado;
               
        produtoItemsList.push({...osProduto})

        setFieldsValue({producao: { produtoItemsList } }, () => {
            setFieldsValue({
                osProduto: { 
                    id: null,
                    produto: { id: null },
                    quantidade: 1,
                }
            })
        })

        this.setState({ produtoDescricao: null, viewStateTab: INSERTING })
    }    

    prepareUpdate = (record) => {
        const { form: { setFieldsValue } } = this.props
        setFieldsValue({ osProduto: {...record, 
        } } )
        this.setState({ viewStateTab: EDITING, produtoDescricao: record.produto.nome })
    }    
    
    remover = (record, { getFieldValue, setFieldsValue }) => {
        let produtoItemsList = getFieldValue("producao.produtoItemsList")

        produtoItemsList.splice(produtoItemsList.findIndex((item) => {            
            return (item.produto && item.produto.id === record.produto.id)
        }), 1)

        setFieldsValue({producao: { produtoItemsList } })
    }

    // handleChangeProduto = (idProduto) => {    
    //     const { form: { getFieldsValue }  } = this.props    
    //     const { osProduto } = getFieldsValue() 
    //     const { percDesconto, quantidadeUnidade, quantidadeCaixa } = osProduto 
        
    //     this.alterandoValores(idProduto, percDesconto)
    // }  

    // alterandoValores = (idProduto, percDesconto, quantidadeUnidade, quantidadeCaixa) => {
    //     const { form: { getFieldsValue, setFieldsValue, getFieldValue }, 
    //         produtoList = [], tabelaPrecoProdutoList = [] 
    //     } = this.props 
    //     const { osProduto } = getFieldsValue()

    //     let produto = produtoList.find(c=> c.id == idProduto);
    //     let idTabelaPreco = getFieldValue("producao.tabelaPreco.id") 
    //     let fracionado = produto.fracionado
    //     let valorVendaUnidade = produto.valorVendaUnidade
    //     let valorVendaCaixa = produto.valorVendaCaixa   

    //     if (!isNil(idTabelaPreco)){
    //         let tabelaPrecoProduto = tabelaPrecoProdutoList.find(c=> c.idTabelaPreco == idTabelaPreco && c.produto.id == idProduto);

    //         if (!isNil(tabelaPrecoProduto)){
    //             if (tabelaPrecoProduto.valorUnidade) {
    //                 valorVendaUnidade = tabelaPrecoProduto.valorUnidade
    //             }

    //             if (tabelaPrecoProduto.valorCaixa) {
    //                 valorVendaCaixa = tabelaPrecoProduto.valorCaixa
    //             }
    //         }
    //     }

    //     let vValorDesconto = 0;
    //     let totalProduto = 0;

    //     if (fracionado) {
    //         vValorDesconto = obterValorDesconto(percDesconto, valorVendaUnidade); 
    //         totalProduto = (valorVendaUnidade - vValorDesconto) * (isNil(quantidadeUnidade) ? produto.quantidadeCaixa : quantidadeUnidade);
    //     } else {
    //         vValorDesconto = obterValorDesconto(percDesconto, valorVendaCaixa);
    //         totalProduto = (valorVendaCaixa - vValorDesconto) * (isNil(quantidadeCaixa) ? 1: quantidadeCaixa); 
    //     }

    //     setFieldsValue({osProduto: {
    //             ...osProduto, 
    //             //quantidade: isNil(quantidade) ? produto.quantidadeCaixa : quantidadeUnidade,
    //             quantidade: isNil(quantidade) ? 1 : quantidade,
    //             quantidadeNaCaixa: isNil(quantidadeNaCaixa) ? 1 : quantidadeNaCaixa,
    //             //qtdEstoqueCaixa: produto.qtdEstoqueCaixa,
    //             //qtdEstoqueUnidade: produto.qtdEstoqueUnidade,
    //             fracionado: fracionado,
    //         } 
    //     })

    //     this.setState({produtoDescricao: produto.nome})
    // }

    // getTotal = (idProduto, quantidadeUnidade, quantidadeCaixa, desconto, fracionado, valorVendaUnidade, valorVendaCaixa) => {
    //     const { form: { getFieldsValue, }, produtoList = [], tabelaPrecoProdutoList = [] } = this.props
    //     const { osProduto } = getFieldsValue() 

    //     let produto = produtoList.find(c=> c.id == idProduto);
    //     let tabelaPrecoProduto = null;
    //     let totalProduto = 0;

    //     if (produto.fracionado) {
    //         totalProduto = (produto.valorVendaUnidade - desconto) * quantidadeUnidade;
    //     } else {
    //         totalProduto = (produto.valorVendaCaixa - desconto) * quantidadeCaixa; 
    //     }

    //     return totalProduto;
    // }
    
    // onChangePercDesconto = (percDesconto) => {   
    //     const { form: { getFieldsValue, setFieldsValue } } = this.props    
    //     const { osProduto } = getFieldsValue()     
    //     const { produto: {id: idProduto}, quantidadeUnidade, quantidadeCaixa
    //     } = osProduto
       
    //     this.alterandoValores(idProduto, percDesconto, quantidadeUnidade, quantidadeCaixa) 
    // }  
    
    // onChangeDesconto = (desconto) => {    
    //     const { form: { getFieldsValue, setFieldsValue }, produtoList = [] } = this.props    
    //     const { osProduto } = getFieldsValue()     
    //     const { produto: {id: idProduto}, quantidadeUnidade, quantidadeCaixa, valorUnidade, valorCaixa, fracionado } = osProduto
        
    //     let percDesconto = 0;

    //     if (fracionado) {
    //         percDesconto = obterPercentualDesconto(desconto, valorUnidade);
    //     } else {
    //         percDesconto = obterPercentualDesconto(desconto, valorCaixa);
    //     }

    //     this.alterandoValores(idProduto, percDesconto, quantidadeUnidade, quantidadeCaixa) 
    // }   
    
    // onChangeQtdCaixa = (qtdCaixa) => {    
    //     const { form: { getFieldsValue, setFieldsValue }, produtoList = [], } = this.props    
    //     const { osProduto } = getFieldsValue()     
    //     const { produto: {id: idProduto}, quantidadeCaixa, quantidadeUnidade, desconto, percDesconto } = osProduto

    //     this.alterandoValores(idProduto, percDesconto, quantidadeUnidade, qtdCaixa)
    // }

    // onChangeQtdUnidade = (quantidadeUnidade) => {    
    //     const { form: { getFieldsValue, setFieldsValue }, produtoList = [], } = this.props    
    //     const { osProduto } = getFieldsValue()     
    //     const { produto: {id: idProduto}, percDesconto, quantidadeCaixa} = osProduto

    //     this.alterandoValores(idProduto, percDesconto, quantidadeUnidade, quantidadeCaixa)
    // } 

    limpar = () => {
        const { form: { getFieldsValue, setFieldsValue }, } = this.props
        const fields = getFieldsValue()
        fields.osProduto = {
            id: null,
            produto: { id: null},
            quantidade: 1, 
        }

        this.setState({ viewStateTab: INSERTING })
        setFieldsValue(fields)
    }   

    getExtra() {
        const { viewStateTab } = this.state
        const { stateView, form: { getFieldValue }, producao = {}, } = this.props

        return (
            <>
                <Button 
                    type={"primary"} 
                    onClick={() => this.adicionar()}>
                    { isEqual(viewStateTab, INSERTING) ? 'Adicionar' : 'Atualizar' } Produto
                </Button>
                &nbsp;
                <Button 
                    type={"primary"} 
                    onClick={this.limpar} 
                    disabled= {isEqual(stateView, VIEWING)}>
                    Limpar
                </Button>
            </>
        )
    }

    render() {
        const { viewStateTab, produtoDescricao } = this.state
        const { 
            form,        
            produtoList = [],
            producao = {},      
            stateView,  
        } = this.props
        const { produtoItemsList = [] } = producao || {}
        const { getFieldDecorator, getFieldValue } = form

        let produtoListFilter = produtoList.filter(c=> c.tipo == 'P' || c.tipo == 'C')
        const idForm = getFieldValue("osProduto.id") || null  
        //let produto = null;  
        
        // if (idProdutoForm) {        
        //     produto = produtoList.find(c=> c.id == idProdutoForm);
        // }

        const produtoNome = getFieldValue("osProduto.produto.nome") || produtoDescricao
        
        return (<div>
            <Card title={"Informe os dados referente aos produtos da Ordem de Serviço"} extra={this.getExtra()}>
                { getFieldDecorator("osProduto.id", { initialValue: idForm })(<Input type="hidden" />) }
                { getFieldDecorator("osProduto.produto.nome", { initialValue: produtoNome })(<Input type="hidden" />) }     
                
                <Row gutter = { 12 }>
                    <Col span = { 6 }>
                        <Form.Item label={"Produto"}>
                            {
                                getFieldDecorator('osProduto.produto.id', {})(
                                    <Select 
                                        showSearch
                                        optionFilterProp="children"
                                        placeholder={"Digite para buscar"}
                                        //onChange={(value) => this.handleChangeProduto(value)}
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        disabled= {isEqual(stateView, VIEWING)}
                                        >
                                        {generateOptions(produtoListFilter.map(({id, nome}) => ({id, descricao: nome})))}
                                    </Select>
                                )
                            }
                        </Form.Item>               
                    </Col>
                    <Col span={2}>
                        <Form.Item label={"Quantidade"}>
                            {
                                getFieldDecorator('osProduto.quantidade', {
                                    initialValue: 1
                                })(
                                    <InputNumber style={{ width: "150" }} 
                                        min={0}
                                        precision={0}
                                        step={1}  
                                        //onChange={(value) => this.onChangeQtdCaixa(value)} 
                                        disabled= {isEqual(stateView, VIEWING)}
                                    />
                                )
                            }
                        </Form.Item>            
                    </Col> 
                    {/* <Col span={2}>
                        <Form.Item label={"Estoque"}>
                            {
                                getFieldDecorator('osProduto.qtdEstoqueCaixa', {
                                    initialValue: 0
                                })(
                                    <InputNumber 
                                        style={{ width: "150" }}
                                        min={0}
                                        precision={2}
                                        step={1}
                                        disabled
                                    />
                                )
                            }
                        </Form.Item>
                    </Col>                      */}
                </Row>  
                <Row gutter = { 12 }>
                    <Form.Item 
                        //label={"Produtos"}
                        >
                        {
                            getFieldDecorator('producao.produtoItemsList', {
                                rules: [{ required: false, type: 'array', message: 'Por favor, informe pelo menos um produto.'}],
                                initialValue: [...produtoItemsList],
                                valuePropName: 'dataSource'
                            })(
                                <Table rowKey={(row) => row.id || row.produto && row.produto.id} size={"small"} 
                                    pagination={false} bordered
                                    title={() => <span>PRODUTOS</span>}
                                    footer={() => 
                                        <Row>
                                            <Col span={4}>
                                               {"Total quantidade:" + produtoItemsList.reduce((acum, {quantidade}) => acum + Number(quantidade), 0) }
                                            </Col>
                                        </Row>
                                    }>
                                    <Table.Column title={<center>Produto</center>} key={"nomeProduto"} dataIndex={"nomeProduto"} align={"center"} />  
                                    <Table.Column title={<center>Quantidade</center>} key={"quantidade"} dataIndex={"quantidade"} align={"center"} />    
                                    {/* <Table.Column title={<center>Qtd. unids.</center>} key={"quantidadeUnidade"} dataIndex={"quantidadeUnidade"} align={"center"} /> */}
                                    {/* <Table.Column title={<center>Valor c/ desc.</center>} key={"total2"} dataIndex={"total2"} align={"center"}
                                        render={(text, record) => record.fracionado ?
                                             (record.valorUnidade - record.desconto) : 
                                             (record.valorCaixa - record.desconto)  } /> */}
                                    <Table.Column title={<center>Ações</center>} key={"actions"} 
                                                dataIndex={"actions"} 
                                                align={"center"} 
                                                render={ (text, record) => {
                                                    return (
                                                        <span>
                                                            {
                                                                // !record.id &&
                                                                !isEqual(stateView, VIEWING) &&
                                                                <>
                                                                <Icon style={{cursor: 'pointer'}} type={"delete"} onClick={ () => this.remover(record, form) }/>
                                                                <Divider type="vertical"/>
                                                                </>
                                                            }
                                                            {
                                                                record.id && !isEqual(stateView, VIEWING) &&
                                                                <>                                                    
                                                                    <Icon style={{cursor: 'pointer'}} type={ 'edit' } onClick={(e) => this.prepareUpdate(record)}></Icon> 
                                                                </>
                                                            }
                                                        </span>
                                                    )}
                                                }/>
                                </Table>
                            )
                        }
                    </Form.Item>
                </Row>
            </Card>
        </div>)
    }
}
