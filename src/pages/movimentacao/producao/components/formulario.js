import React, { Component } from 'react'
import { Card, Row, Button, Form, Spin, Tabs, Icon, Input, Modal, Col, Avatar, Divider, Tooltip } from 'antd'
import { isEqual, isNil, isEmpty, get } from 'lodash'
import { SEARCHING, INSERTING, VIEWING } from '../../../util/state'
import { connect } from 'react-redux'
import Actions from '../redux'
import TabDados from './tabDados'
import TabProduto from './tabProduto'
import { getTitle } from '../../../util/helper'
import { openNotification } from '../../../util/notification'
import { getCard } from '../../../util/miniCard'

const { Meta } = Card

class Formulario extends Component {
    
    constructor(props){
        super(props)
        this.state = { activeKey: "1" }
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        const message = get(nextProps, ['message'], "")

        if (!isEmpty(message)){
            openNotification(message)
        
            if (isEqual(message.tipo, 'success')) {
                this.props.cleanTable()
                this.props.setStateView(SEARCHING)                
                this.handleReset()
                this.props.setProducao(null)                
            }

            this.props.cleanMessage()
        }
    }

    componentDidMount() {
        const { producao, obter } = this.props
        const { id } = isNil(producao) ? {} : producao        

        if(!isNil(id)){
            obter(id)
        }
    }

    setActiveKey = (activeKey) => this.setState({activeKey})

    getExtra = () => {
        const { stateView } = this.props

        return (
            <div>
                <Button 
                    type={ "primary"} 
                    onClick={this.voltar}
                    style={{marginRight: '10px'}}>
                        Voltar
                </Button>
                <Divider type="vertical" />
                <Button 
                    type={"primary"}
                    disabled= {isEqual(stateView, VIEWING)}                             
                    onClick={this.handleSubmit}
                    >                            
                    { this.isSaving() ? 'Salvar' : 'Atualizar' } Produção
                </Button>
            </div>
        )
    }

    render() {
        const { activeKey } = this.state
        const { 
            fetching, 
            producao, 
            form, 
            stateView, 
        } = this.props
        const { getFieldDecorator, getFieldValue } = form
        const { 
            id, 
            idUsuarioInclusao, 
            status,
            cancelado, 
            produtoItemsList = [], 
        } = isNil(producao) ? {} : producao

        let produtoItemsListForm = isNil(getFieldValue("producao.produtoItemsList")) ? produtoItemsList : getFieldValue("producao.produtoItemsList");

        //let totalProduto = produtoItemsListForm.filter(c=> c.bonificacao == false).reduce((acum,{valor, quantidade}) => acum + (Number(quantidade) * Number(valor)), 0);
        //let totalProdutoMeldica = produtoItemsListForm.filter(c=> c.bonificacao == false && c.idEmpresaProduto != 2).reduce((acum, {total}) => acum + total, 0);        
        //let totalProdutoCosmetico = produtoItemsListForm.filter(c=> c.bonificacao == false && c.idEmpresaProduto == 2).reduce((acum, {total}) => acum + total, 0);
        //let totalPedido = (totalProdutoMeldica ? totalProdutoMeldica : 0) + (totalProdutoCosmetico ? totalProdutoCosmetico : 0) +  (totalProdutoKit ? totalProdutoKit : 0) + (totalFrete ? totalFrete : 0);
        
        return (
            <Spin spinning={fetching}>
              <Form onSubmit={this.handleSubmit} >
                <Card title={ getTitle(`${isEqual(stateView, VIEWING) ? 'Visualizando' : (this.isSaving() ? 'Cadastro' : 'Edição')} Pedido`) } 
                    extra={this.getExtra()}>                    
                    { getFieldDecorator("producao.id", { initialValue: id })(<Input type="hidden" />) }
                    { getFieldDecorator("producao.idUsuarioInclusao", { initialValue: isNil(idUsuarioInclusao) ? null : idUsuarioInclusao})(<Input type="hidden" />) }
                    { getFieldDecorator("producao.status", { initialValue: isNil(status) ? 'A' : status})(<Input type="hidden" />) }
                    { getFieldDecorator("producao.cancelado", { initialValue: isNil(cancelado) ? false : cancelado})(<Input type="hidden" />) }
                    {/* <Row gutter={2}>
                        <Col span={ 3 }>
                        {
                            getCard(`${id ? 'Nota: ' + id : 'Status Nota'}`, '#6BD098', 'file-protect', statusNovaDescricao ? statusNovaDescricao : 'ABERTA', false)
                        }   
                        </Col>                                  
                        <Col span={ 3 }>
                        {
                            getCard('Produtos(Naturais)', '#FBC658', 'code-sandbox', totalProdutoMeldica.toFixed(2))
                        }
                        </Col>
                        <Col span={ 3 }>
                        {
                            getCard('Produtos(Cosmético)', '#FBC658', 'code-sandbox', totalProdutoCosmetico.toFixed(2))
                        }
                        </Col>                        
                        <Col span={ 3 }>
                        {
                            getCard('Total frete', '#6BD098', 'car', totalFrete ? totalFrete.toFixed(2) : 0)
                        }
                        </Col>
                        <Col span={ 3 }>
                            <Tooltip title="Produtos Naturais + Produtos Cosméticos + frete"> 
                            {
                                getCard('Total a pagar', '#6BD098', 'dollar', totalPedido ? totalPedido.toFixed(2) : 0)
                            }
                            </Tooltip>
                        </Col>                            
                    </Row>   
                    <Row gutter={2}>
                        <Col span={ 3 }>
                        {
                            getCard('Total peso', '#FBC658', 'arrow-down', totalPeso ? totalPeso.toFixed(2) : 0, true, false)
                        } 
                        </Col>
                    </Row>                  */}
                    {/* <Divider /> */}
                    <Row>
                        <TabDados {...this.props} 
                            />
                    </Row>
                    <Row>
                        <TabProduto {...this.props} />
                    </Row>
 
                    <Divider />                  
                    <Row style={{textAlign: "right"}}>
                        <Button 
                            type={ "primary"} 
                            onClick={this.voltar}
                            style={{marginRight: '10px'}}>
                                Voltar
                        </Button>
                        <Button 
                            type={"primary"}
                            disabled= {isEqual(stateView, VIEWING)}                             
                            onClick={this.handleSubmit}
                            >                            
                            { this.isSaving() ? 'Salvar' : 'Atualizar' } Produção
                        </Button>
                    </Row>
                </Card>
            </Form>
        </Spin>
        )
    }

    isSaving = () => isEqual(this.props.stateView, INSERTING)

    voltar = () => {
        const { confirm } = Modal;
        const { setStateView } = this.props;

        confirm({
            title: 'Tem certeza que deseja sair?',
            content: 'Os dados informados serão perdidos.',
            onOk() {
                setStateView(SEARCHING);
            },
            onCancel() {
              console.log('Cancelar');
            },
        });
    }

    handleReset = () => {
        this.props.form.resetFields()
    }

    handleSubmit = e => {
        const { kitProdutoList = []} = this.props

        e.preventDefault();
        this.props.form.validateFields((err, { producao }) => {
            if (!err) {         
                const { produtoItemsList = [] } = producao
 
                this.props.setProducao(producao)
                this.props.salvar(producao)
                
                this.setActiveKey('1')
            } else {
                openNotification({tipo: 'warning', descricao: 'Existem campos obrigatórios a serem preenchidos.'})
            }
        });
    };
    
}

const mapStateToProps = (state) => {
    return {
        ...state.producao.data,
        producao: state.producao.producao,
        stateView: state.producao.stateView,
        fetching: state.producao.fetching,  
        profile: state.login.data.profile,      
    }
}

const mapDispatchToProps = (dispatch) => ({
    obter: (id) => dispatch(Actions.producaoObter(id)),    
    cleanMessage: ()  => dispatch(Actions.producaoCleanMessage()),
    cleanTable: () => dispatch(Actions.producaoCleanTable()),
    setStateView: (stateView) => dispatch(Actions.producaoSetStateView(stateView)),
    setProducao: (producao) => dispatch(Actions.producaoSetProducao(producao)),    
    salvar: (obj) => dispatch(Actions.producaoSalvar(obj)),
})

const wrapedFormulario = Form.create()(Formulario)
export default connect(mapStateToProps, mapDispatchToProps)(wrapedFormulario)