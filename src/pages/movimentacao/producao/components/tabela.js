import React, { Component } from 'react'
import { Card, Table , Icon, Divider, Tooltip, Button, Modal, Popover, Badge  } from 'antd'
import { EDITING, VIEWING } from '../../../util/state'
import { connect } from 'react-redux'
import Actions from '../redux'
import Pagination from '../../../util/Pagination'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { getTitle, getTagStatusNota } from '../../../util/helper'
import { hasAnyAuthority, getUser } from '../../../../services/authenticationService'
import Workbook from 'react-excel-workbook'
import { openNotification } from '../../../util/notification'
import { isEqual, isEmpty, get } from 'lodash'

const Sheet = Workbook.Sheet
const Column = Workbook.Column

class Tabela extends Component {

    // getExtra = (length) => {
    //     if (length && length > 0) {
    //         return (
    //             <div>
    //                 <Workbook filename='producaos.xlsx'
    //                     element={
    //                         <Tooltip title='Click para baixar os registros.' placement='left'>
    //                             <Button type='primary' shape='circle' size='small' icon='download' />
    //                         </Tooltip>
    //                     }>
    //                     <Sheet data={this.props.list || []} name='producaos'>
    //                         {/* <Column label='Nome' value={row => row.nome ?  row.nome : '' } /> */}
    //                         <Column label='Valor' value={row => row.valor ? row.valor : ''} />
    //                     </Sheet>
    //                 </Workbook>
    //             </div>)
    //     }
    // }

    setModo = (producao, stateView) => {
        this.props.setStateView(stateView)
        this.props.setProducao(producao)
    }

    getAcoes = (record) => {
        //console.log(getUser())
        const { funcionario = {}, perfil = {}, } = getUser()
        const { id: idFuncionario } = funcionario || {}

        return (
            <div>
                {
                <>
                    {
                    hasAnyAuthority("PRODUCAO_ALTERAR") &&
                    <Tooltip title="Editar Pedido">
                        <Icon style={{cursor: 'pointer'}}
                            type={ 'edit' }
                            className={'tabela-icone'}
                            onClick={(e) => this.setModo(record, EDITING)}></Icon>
                    </Tooltip>
                    }

                    <Divider type="vertical"/>
                    {/* <Tooltip title="Imprimir pedido">
                        <Icon
                            className={'tabela-icone'}
                            type={ 'printer' }
                            onClick={(e) => this.props.imprimir({ ...record, tipoRelatorio: "T" })}
                            />
                    </Tooltip>   */}
                </>
                }
            </div>
        )
    }

    render() {
        const { list = [] , remover } = this.props
        return (
            list.length > 0 &&
            <Card 
                title={ getTitle("Listagem") } 
                //extra={ this.getExtra(list.length)} 
                >
                <Table rowKey={ (row) => row.id}
                        dataSource={list}
                        size={"middle"}
                        pagination={Pagination()}>
                    {/* <Table.Column key={'id'}
                                    dataIndex={'id'}
                                    title={'Número'}
                                    align={ "left" }
                                    //width={'8%'}
                                    /> */}
                    <Table.Column key={'dataInclusao'}
                                    dataIndex={'dataInclusao'}
                                    title={'Data'}
                                    align={ "center" }
                                    render={(text) => text && moment(text, 'YYYY-MM-DD').format('DD/MM/YYYY')}
                                    //width={'10%'}
                                    />
                    {/* <Table.Column key={'statusNovaDescricao'}
                                    //dataIndex={'statusNovaDescricao'}
                                    dataIndex={'statusNota'}
                                    title={'Status Nota'}
                                    align={ "left" }
                                    render={(text) => getTagStatusNota(text,false)}/> */}
                    {/* <Table.Column key={'funcionario.nome'}
                                    dataIndex={'funcionario.nome'}
                                    title={'Vendedor'}
                                    align={ "left" }/> */}
                    <Table.Column key={'acoes'}
                                    dataIndex={'acoes'}
                                    title={'Ações'}
                                    align={ "center" }
                                    width={'15%'}
                                    render={(text, record) => this.getAcoes(record) }
                                    />
                </Table>
            </Card>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.producao.data,
        fetching: state.producao.fetching,
        stateView: state.producao.stateView,
        producao: state.producao.visita
    }
}

const mapDispatchToProps = (dispatch) => ({
    remover: (id) => dispatch(Actions.producaoRemover(id)),
    setStateView: (stateView) => dispatch(Actions.producaoSetStateView(stateView)),
    setProducao: (producao) => dispatch(Actions.producaoSetProducao(producao)),
    cleanMessage: ()  => dispatch(Actions.producaoCleanMessage()),
    cleanTable: () => dispatch(Actions.producaoCleanTable()),
    imprimir: (obj) => dispatch(Actions.producaoImprimir(obj)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tabela)