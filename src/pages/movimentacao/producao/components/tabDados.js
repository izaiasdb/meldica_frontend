import React from 'react'
import { Row, Col, Form, Select, Input, DatePicker, InputNumber, Switch, Card, Button, Icon, Tooltip  } from 'antd'
import { generateOptions } from '../../../util/helper'
import { isNil, isEqual } from 'lodash'
import moment from 'moment'
import { validarCPF } from '../../../util/validacaoUtil'
import NumericInput from '../../../util/numericInput'
import { VIEWING } from '../../../util/state'
import { getUser } from '../../../../services/authenticationService'

const Option = Select.Option

const TabDados = (props) => {
    const validateDataProducao = function(rule, value, callback) {
        const { form } = this.props
        //const dataMovimentacao = form.getFieldValue("producao.dataVenda")
        let now = moment();

        if(isNil(value)){
            callback("Por favor, preencha o campo 'Data da producao.'")
        }

        if(value.isSameOrAfter(now)) {
            callback("A Data da producao não pode ser uma data futura.")
        }
    }

    const { 
        form: { getFieldDecorator, getFieldValue, getFieldsValue },
        producao = {},
        funcionarioList = [],
        stateView,
    } = props
    const {
        //funcionario = {},
        dataProducao,
        observacao, 
    } = producao || {}
    const { funcionario: funcionarioUser} = getUser()
    const { id: idFuncionarioUser } = funcionarioUser || {}

    const toInputUppercase = e => { e.target.value = ("" + e.target.value).toUpperCase(); };

    return (<div>
        <Card title={"Informe os dados referente ao Pedido"}>
            <Row gutter={ 12 }>
                <Col span={ 3 }>
                    <Form.Item label={"Data de produção"}>
                        {
                            getFieldDecorator('producao.dataProducao', {
                                rules: [{required: true, message: "Por favor, informe a data de produção."}
                            ], initialValue: isNil(dataProducao) ? moment() : new moment(dataProducao)
                            })(
                                <DatePicker 
                                    style = {{ width: '98%' }}
                                    //disabled= {isEqual(stateView, VIEWING)}
                                    format={'DD/MM/YYYY'}/>
                            )
                        }
                    </Form.Item>                
                </Col>         
            </Row>              
            {/* <Row gutter={12}>
                <Col span={ 8 }>
                    <Form.Item label={"Vendedor"}>
                        {
                            getFieldDecorator('producao.funcionario.id', {
                                rules: [{required: true, message: 'Por favor, informe o Vendedor.'}],
                                initialValue: !isNil(funcionario) && !isNil(funcionario.id) ? funcionario.id : idFuncionarioUser
                            })(
                            <Select 
                                showSearch
                                optionFilterProp="children"
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                disabled= {isEqual(stateView, VIEWING)}
                                >
                                <Option key={1} value={null}>{"Selecione"}</Option>
                                {generateOptions(funcionarioList)}
                            </Select>
                            )
                        } 
                    </Form.Item>
                </Col> 
            </Row>    */}
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item label={"Observação"}>
                        {
                            getFieldDecorator('producao.observacao', {
                                rules:[
                                    {required: false, whitespace: true, message: 'A Observação é obrigatória.'},
                                    {required: false, max: 800, message: 'A quantidade máxima de caracteres é 800.'}
                                ],
                                initialValue: observacao || null
                            })(<Input.TextArea 
                                    autoSize={{ minRows: 5, maxRows: 8 }} 
                                    onInput={toInputUppercase}
                                    disabled= {isEqual(stateView, VIEWING)}
                                    placeholder={"Coloque alguma observação"} />)
                        }  
                    </Form.Item>                          
                </Col>
            </Row>   
        </Card>      
    </div>)
}

export default TabDados