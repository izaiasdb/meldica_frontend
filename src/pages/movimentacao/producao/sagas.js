import { call, put, select } from 'redux-saga/effects'
import Actions from './redux';
import { get } from "lodash";
import { getUser } from '../../../services/authenticationService'
import moment from 'moment';

export function * fetch (api)  {
  try {    
      const response = yield call(api.Producao.init)
      if (response.ok) {
        const data = get(response, ['data'], {})
        yield put(Actions.producaoSuccess(data))
      } else {
        const { message } = get(response, ['data'], {})
        yield put(Actions.producaoFailure(message))
     }
  } catch (ex) {
    console.log(ex)
    yield put(Actions.producaoFailure())
  }
}

export function * obter(api, { id })  {
  try {    
    const response = yield call(api.Producao.obter, id)

    if (response.ok) {
      const producao = get(response, ['data'], {})
      console.log(producao)
      yield put(Actions.producaoSetProducao(producao))
    } else {
      const { message } = get(response, ['data'], {})
      yield put(Actions.producaoFailure(message))
    }
  } catch (ex) {
    console.log(ex)
    yield put(Actions.producaoFailure())
  }
}

export function * pesquisar (api, { producao })  {
  try {    
    const response = yield call(api.Producao.pesquisar, producao)
      if (response.ok) {
        const list = get(response, ['data'], {})
        yield put(Actions.producaoSuccess({list}))
      } else {
        const { message } = get(response, ['data'], {})
        yield put(Actions.producaoFailure(message))
     }
  } catch (ex) {
    console.log(ex)
    yield put(Actions.producaoFailure())
  }
}

export function * salvar (api, { obj })  {
  try {
    const { id } = getUser();
    obj.idUsuarioAlteracao = id;

    if (!obj.id){
      obj.idUsuarioInclusao = id;
    }

    const response = yield call(api.Producao.salvar, {...obj })
    
    if (response.ok) {
      yield put(Actions.producaoSuccess({
        message: {
          tipo: 'success', descricao: `Registro ${obj && obj.id ? 'alterado' : 'salvo'} com sucesso.`
        }
      }))
    } else {
      const { message } = get(response, ['data'], {})
      yield put(Actions.producaoFailure(message))
    }
  } catch (ex) {
    console.log(ex)
    yield put(Actions.producaoFailure())
  }
}
export function* imprimir(api, { obj }) {
  try {
    const response = yield call(api.Producao.imprimir, {...obj })
    
    if (response.ok) {
      const report = get(response, ['data'], {})
      const bytes = yield stringToBytes(report);
      const extensao = 'PDF'
      //const { unidadeAtual = {} } = yield select(state => state.login.data.profile)
      const date = new moment().format('DDMMYYYY_HHmmss')
      yield saveByteArray(extensao === 'PDF' ? `Pedido_${date}.pdf` : `Pedido_${date}.xls`, bytes, extensao);
      yield put(Actions.producaoSuccess({}))
    } else {
      const { message } = get(response, ['data'], {})
      yield put(Actions.producaoFailure(message))
    }
  } catch (ex) {
    console.log(ex)
    yield put(Actions.producaoFailure())
  }
}

function* stringToBytes(base64) {
  var binaryString = window.atob(base64)
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
    var ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}

function* saveByteArray(reportName, byte, extensao) {
  let blob = null;
  if (extensao === 'PDF') {
    blob = new Blob([byte], { type: "application/pdf" });
  } else {
    blob = new Blob([byte]);
  }
  let link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  link.download = reportName;
  link.click();
};